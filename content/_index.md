---
title: The level editor
---

## Get started

**[Download NetRadiant](/page/download)** and map like a boss!

## You are the community

NetRadiant is a brush-based 3D game level editor with support for many games like [Xonotic](https://xonotic.org), [Unvanquished](https://unvanquished.net), and more…

{{< gallery caption-effect="fade" >}}
  {{< figure thumb=".thumb" link="/img/screenshots/20200119-223235-000.netradiant-xonotic.png" caption="Xonotic map in NetRadiant">}}
  {{< figure thumb=".thumb" link="/img/screenshots/20200119-223626-000.netradiant-unvanquished.png" caption="Unvanquished map in NetRadiant" >}}
  {{< figure thumb=".thumb" link="/img/screenshots/20200119-225037-000.netradiant-smokinguns.png" caption="Smokin' Guns map in NetRadiant" >}}
{{< /gallery >}}

Maintained by a community of gamers, mappers, modders and game developpers, NetRadiant is free and open source and run on various operating systems like Linux, Windows and FreeBSD. macOS user? there is a solution for you, [learn more…](/page/about)
