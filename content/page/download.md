---
title: Download NetRadiant
subtitle: Dowload and start mapping!
comments: false
authors:
 - xonotic
 - illwieckz
image: /img/screenshots/20200120-020950-000.netradiant-windows-build.png
---

![NetRadiant Windows build](/img/screenshots/20200120-020950-000.netradiant-windows-build.png)

## Xonotic 0.8.2 Mapping Support package

Xonotic Mapping Support package provides required assets to map for the Xonotic game plus a NetRadiant build for Windows. This build is pretty outdated so see below for newer builds.

- [Download **Xonotic 0.8.2 Mapping Support**](http://dl.xonotic.org/xonotic-0.8.2-mappingsupport.zip).


## illwieckz's NetRadiant builds

Those are built from time to time by [illwieckz](/author/illwieckz), in an attempt to provide up-to-date NetRadiant builds.

- [Download **NetRadiant 1.5.0-20200419 for Linux 64-bit**](https://dl.illwieckz.net/b/netradiant/build/preversions/netradiant_1.5.0-20200419-illwieckz-linux-amd64.tar.xz), \
it's expected users have an already working GTK2 and OpenGL environment[^1], you may have to start NetRadiant from a console[^2].
- [Download **NetRadiant 1.5.0-20200419 for Windows 64-bit**](https://dl.illwieckz.net/b/netradiant/build/preversions/netradiant_1.5.0-20200419-illwieckz-windows-amd64.zip), \
such Windows build is reported to work on **macOS** using [Wine](https://wiki.winehq.org/MacOS).
- [Download **NetRadiant 1.5.0-20200419 for Windows 32-bit**](https://dl.illwieckz.net/b/netradiant/build/preversions/netradiant_1.5.0-20200419-illwieckz-windows-i686.zip).

With NetRadiant is provided various gamepacks for many games including [Xonotic](https://xonotic.org), [Unvanquished](https://unvanquished.net) and many others. The `daemonmap` tool for Unvanquished navmesh generation is included.

Those builds are now to be prefered over Ingar's builds[^3].

People with old Intel chip on Windows 10 may face OpenGL issue and unusable white 3D/2D views, those people can workaround the issue by moving everything from `mesa3d/` subdirectory right to `netradiant.exe` to enable software rendering fallback.

See the [related forum thread](https://forums.xonotic.org/showthread.php?tid=8108) for more information about those builds.

[^1]: To get a proper GTK2 with OpenGL environment on Linux, install package `libgtkglext1` on Ubuntu, `gtkglext-libs` on Fedora, `gtkglext` on Arch and Solus, `libgtkglext-x11-1_0-0` on openSUSE, or `x11-libs/gtkglext` on Gentoo. If you run another distribution, look for the _GtkGLExt_ package, this must also bring required dependencies. Other libraries are bundled.

[^2]: On Linux, you may not be able to run NetRadiant by double-clicking on the icon (that's an issue [in the desktop software](https://gitlab.freedesktop.org/xdg/shared-mime-info/issues/11)), you may have to open a terminal console first to run the binary from the command line.

[^3]: Ingar [provided](http://ingar.intranifty.net/gtkradiant/) NetRadiant builds and gamepacks for a long time. Those builds are now severly outdated and newer builds are now to be preferred instead.
