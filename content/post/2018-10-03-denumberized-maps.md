---
title: Denumberized maps for Git lovers
date: 2018-10-03
comments: false
authors:
 - illwieckz
image: /img/screenshots/20180505-200637-000.netradiant-no-map-number-comments.png
---

![Disable map numbering](/img/screenshots/20180505-200637-000.netradiant-no-map-number-comments.png)

By default, NetRadiant writes map with a lot of comment telling the entity or brush number. This is a legacy behavior that may be useful for debugging, comments like that:

```
// entity 0
```
```
// brush 249
```

Unfortunately, this does not integrate well with source version control tools, as a simple modification in a map can renumber a thousands of lines, making diff unreadable and marge painful.

An option was added to make the behavior disableable by the user. While the feature is kept enabled by default as it was legacy behavior, disable it and make your VCS happy.
