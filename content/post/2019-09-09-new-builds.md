---
title: New builds
date: 2019-08-08
comments: false
authors:
 - illwieckz
image: /img/screenshots/20200120-020950-000.netradiant-windows-build.png
---

![NetRadiant Windows build](/img/screenshots/20200120-020950-000.netradiant-windows-build.png)

New builds are available, see the [download page](/page/download)!

They're built for Linux and Windows (known to work on Wine on macOS), and are shipped with the `daemonmap` tool for Unvanquished navmesh generation.
