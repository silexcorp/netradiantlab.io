---
title: Pakpath support
date: 2018-01-06
comments: false
authors:
 - illwieckz
image: /img/screenshots/20171231-183646-000.netradiant-pakpath.png
---

![Pakpath](/img/screenshots/20171231-183646-000.netradiant-pakpath.png)

Pakpaths support was just added, NetRadiant can support up to 5 pakpaths and it's also possible to disable engine directoru and home directory to fully control what's loaded or not. Q3map2 got the `-fs_pakpath` option like Dæmon's `-pakpath` and related option to also disable engine and home directory.

Gamepacks may have to be edited to fully benefit fromthe feature, learn more on the related [merge request thread](https://gitlab.com/xonotic/netradiant/merge_requests/79).
